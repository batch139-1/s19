const getCube = (cube) => {
  const cubed = cube ** 3;
  console.log(`The cube of ${cube} is ${cubed}.`);
};

getCube(2);

const address = [`211B`, `Baker St.`, `London`];

const [lot, street, city] = address;

console.log(`I live at ${lot} ${street}, ${city}.`);

const animal = {
  name: `Bogart`,
  breed: `Aspin`,
  weight: 20,
  age: 8,
};

const animalDetails = (animalObject) => {
  const { name, weight, breed, age } = animalObject;

  console.log(
    `${name} is an ${age} year old ${breed} dog weighing at ${weight} kgs.`
  );
};

animalDetails(animal);

let numbers = [1, 2, 3, 4, 5, 15];

numbers.forEach((number) => console.log(number));

let sum = (reduceNumber = () => {
  numbers.reduce((a, b) => {
    let total = a + b;
    return total;
  });
});

console.log(sum);

class dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const Oreo = new dog(`Oreo`, 3, `Corgi`);
console.log(Oreo);
